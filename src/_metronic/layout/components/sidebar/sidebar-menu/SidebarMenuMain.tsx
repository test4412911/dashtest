/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import { useIntl } from 'react-intl'
import { KTSVG } from '../../../../helpers'
import { SidebarMenuItemWithSub } from './SidebarMenuItemWithSub'
import { SidebarMenuItem } from './SidebarMenuItem'

const SidebarMenuMain = () => {
  const intl = useIntl()

  return (
    <>
      <SidebarMenuItem
        to='/'
        icon='/media/icons/duotune/general/gen025.svg'
        title={intl.formatMessage({ id: 'menu.dashboard' })}
        fontIcon='bi-archive'
      />

      <SidebarMenuItem
        to='/subscriptions'
        icon='/media/icons/duotune/general/gen028.svg'
        title={intl.formatMessage({ id: 'menu.subscriptions.explore' })}
        fontIcon='bi-archive'
      />

      <SidebarMenuItem
        to='/settings'
        icon='/media/icons/duotune/coding/cod001.svg'
        title={intl.formatMessage({ id: 'menu.app.settings' })}
        fontIcon='bi-archive'
      />

      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Users</span>
        </div>
      </div>

      <SidebarMenuItem
        to='/dashboard'
        icon='/media/icons/duotune/art/art002.svg'
        title={intl.formatMessage({ id: 'menu.users.explore' })}
        fontIcon='bi-archive'
      />
    </>
  )
}

export { SidebarMenuMain }
