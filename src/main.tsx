import {createRoot} from 'react-dom/client'

import { QueryClient, QueryClientProvider } from "react-query";
import {Chart, registerables} from 'chart.js'


import '^/_metronic/assets/sass/style.scss'
import '^/_metronic/assets/sass/plugins.scss'
import '^/_metronic/assets/sass/style.react.scss'
import {MetronicI18nProvider} from '^/_metronic/i18n/Metronici18n'
import {ReactQueryDevtools} from 'react-query/devtools'
import {AppRoutes} from "^/app/routes/AppRoutes";
import { AuthProvider } from './app/pages/auth/AuthProvider';


Chart.register(...registerables)

const queryClient = new QueryClient()
const container = document.getElementById('root')
if (container) {
  createRoot(container).render(
    <QueryClientProvider client={queryClient}>
      <MetronicI18nProvider>
        <AuthProvider>
          <AppRoutes />
        </AuthProvider>
      </MetronicI18nProvider>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  )
}

if (undefined /* [snowpack] import.meta.hot */) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  undefined /* [snowpack] import.meta.hot */
    .accept();
}
