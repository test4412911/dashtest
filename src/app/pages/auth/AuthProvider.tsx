import { AuthenticationApi, TokenPair, User, UserLevel } from "@digimperial/efoot-auth";
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useIntl } from "react-intl";
import { toast } from "react-toastify";
import { useLogout } from "^/app/queries";

import {
  AuthContext,
  getAccessToken,
  getRefreshToken,
  getUserData,
  persistTokens,
  setUserData,
  useApiAuthroizedConfig,
  useAuthContext,
  useCurrentUser,
} from "^/app/utils";
import { WithChildren } from "^/_metronic/helpers";
import { LayoutSplashScreen } from "^/_metronic/layout/core";

export const AuthProvider: React.FC <WithChildren> = (props) => {
  const { children } = props;

  const [tokens, set] = useState<Partial<TokenPair>>({
    access: getAccessToken(),
    refresh: getRefreshToken(),
  });

  const setTokens = useCallback(
    (tokens: Partial<TokenPair>) => {
      set(tokens);
      persistTokens(tokens);
    },
    [set]
  );

  useEffect(() => {
    const storageListener = () =>
      set({ access: getAccessToken(), refresh: getRefreshToken() });
    window.addEventListener("storage", storageListener);
    return () => window.removeEventListener("storage", storageListener);
  });

  const [currentUser,setCurrentUserCB] = useState<User | undefined>(getUserData());
  const setCurrentUser = useCallback(
    (currentUser: User | undefined) => {
      setCurrentUserCB(currentUser);
    },
    [setCurrentUserCB]
  );

  const logout = () => {
    setTokens({});
    setUserData(undefined);
  }

  const value = useMemo(() => ({ ...tokens, setTokens ,currentUser,setCurrentUser,logout}), [tokens, setTokens,currentUser,setCurrentUser,logout]);

  return (
    <AuthContext.Provider value={value}>
        {children}
    </AuthContext.Provider>
  );
};




export const AuthInit: React.FC<WithChildren> = ({children}) => {
  
  const didRequest = useRef(false);
  const intl = useIntl();
  const [showSplashScreen, setShowSplashScreen] = useState(true)
  // We should request user by authToken (IN OUR EXAMPLE IT'S API_TOKEN) before rendering the application
  const config = useApiAuthroizedConfig();
  const api = useMemo(() => new AuthenticationApi(...config), [config]);
  const {access,logout,setCurrentUser,setTokens} = useAuthContext();
  useEffect(() => {
    const refresh_user = async () => {
      try {
        if (!didRequest.current) {
          const {data} = await api.authMe();
          console.log("hi");
          if(data.user_level !== UserLevel.Admin){
            toast.error(intl.formatMessage({id: "auth.unauthorized"}));
            logout();
            return (didRequest.current = true)
          }
          console.log(data);
          setCurrentUser(data)
        }
      } catch (error) {
        console.error(error)
        if (!didRequest.current) {
          logout()
        }
      } finally {
        setShowSplashScreen(false)
      }

      return () => (didRequest.current = true)
    }
    if (access) {
      refresh_user()
    } else {
      logout();
      setShowSplashScreen(false)
    }

    
    // eslint-disable-next-line
  }, [])

  return showSplashScreen ? <LayoutSplashScreen /> : <>{children}</>
}
