import {Navigate, Routes} from 'react-router-dom'
import { useLogout } from '^/app/queries'

export function Logout() {
  useLogout();
  return (
    <Routes>
      <Navigate to='/auth/login' />
    </Routes>
  )
}
