import { ProfileManagementApi } from "@digimperial/efoot-app";
import { AuthenticationApi, Configuration, UserLevel } from "@digimperial/efoot-auth";
import axios, { AxiosInstance } from "axios";
import { useContext, useMemo } from "react";
import { toast } from "react-toastify";
import { useLogout } from "../queries";

import {
  AuthContext,
  persistTokens,
  useAuthenticationApi,
  useCurrentUser,
} from "./auth";

function useAxios(): AxiosInstance {
  const { access, refresh } = useContext(AuthContext);
  const currentUser = useCurrentUser();
  const api = useAuthenticationApi();
  const authExp = useMemo(() => (currentUser?.exp ?? 0) * 1000, [currentUser]);
  return useMemo(() => {
    const ax = axios.create();
    ax.interceptors.request.use((config) => {
      if (access) {
        if (Date.now() > authExp) {
          if (refresh) {
            return api.authRefreshToken({ access, refresh }).then((res) => {
              const { data } = res;
              persistTokens(data);
              config.headers.Authorization = `Bearer ${data.access}`;
              return config;
            });
          }
        } else {
          config.headers.Authorization = `Bearer ${access}`;
          return config;
        }
      }
      return config;
    });
    return ax;
  }, [authExp]);
}

export type ApiConfig = [Configuration?, string?, AxiosInstance?];

export function useApiConfig(): ApiConfig {
  const ax = useAxios();
  return [undefined, `${import.meta.env.VITE_PUBLIC_API_PREFIX}/admin`, ax];
}

export function useApiAppConfig(): ApiConfig {
  const ax = useAxios();
  return [undefined, `${import.meta.env.VITE_PUBLIC_API_PREFIX}/app`, ax];
}

export function useApiAuthroizedConfig(): ApiConfig {
  const ax = useAxios();
  return [undefined, `${import.meta.env.VITE_PUBLIC_API_PREFIX}`, ax];
}


export function useDOPicture(uuid : string | undefined){
  return import.meta.env.VITE_PUBLIC_ASSETS_PREFIX  + uuid + ".webp";
}