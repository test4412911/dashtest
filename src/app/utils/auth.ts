import {
  AuthenticationApi,
  TokenPair,
  User,
  UserLevel,
} from "@digimperial/efoot-auth";
import React, { Dispatch, SetStateAction, useContext, useMemo } from "react";

const ACCESS_KEY = "AUTH_ACCESS_KEY";
const REFRESH_KEY = "AUTH_REFRESH_KEY";
const USER_KEY = "AUTH_USER_KEY";

export interface TokenClaims {
  user_id: number;
  username: string;
  refresh: boolean;
  user_level: UserLevel;
  aud: string;
  iss: string;
  exp: number;
  iat: number;
  sub: string;
}

export function parseToken(token: string): TokenClaims {
  return JSON.parse(atob(token.split(".")[1]));
}

export function persistTokens(tokens: Partial<TokenPair>) {
  if (tokens.access) {
    localStorage.setItem(ACCESS_KEY, tokens.access);
  } else {
    localStorage.removeItem(ACCESS_KEY);
  }
  if (tokens.refresh) {
    localStorage.setItem(REFRESH_KEY, tokens.refresh);
  } else {
    localStorage.removeItem(REFRESH_KEY);
  }
}
export const getAccessToken = () =>
  localStorage.getItem(ACCESS_KEY) || undefined;
export const getRefreshToken = () =>
  localStorage.getItem(REFRESH_KEY) || undefined;

export function getUserData() {
  if (!localStorage) {
    return;
  }
  const value = localStorage.getItem(USER_KEY) || undefined;
  if (!value) {
    return;
  }

  try {
    const auth: User = JSON.parse(value) as User;
    if (auth) {
      return auth;
    }
  } catch (error) {
    console.error("AUTH LOCAL STORAGE PARSE ERROR", error);
  }
}


export const setUserData = (auth: User | undefined) => {
  if (!localStorage) {
    return
  }
  if (!auth) {
    localStorage.removeItem(USER_KEY)
    return
  }

  try {
    const lsValue = JSON.stringify(auth)
    localStorage.setItem(USER_KEY, lsValue)
  } catch (error) {
    console.error('AUTH LOCAL STORAGE SAVE ERROR', error)
  }
}


interface IAuthContext extends Partial<TokenPair> {
  setTokens: (tokens: Partial<TokenPair>) => void;
  currentUser: User | undefined;
  setCurrentUser: (auth : User | undefined) => void;
  logout: () => void;
}

export const AuthContext = React.createContext<IAuthContext>({
  access: getAccessToken(),
  refresh: getRefreshToken(),
  setTokens: () => {},
  currentUser: getUserData(),
  setCurrentUser: () => {},
  logout: () => {},
});

export const useAuthContext = () => useContext(AuthContext)




export function useCurrentUser(): TokenClaims | undefined {
  const { access } = useContext(AuthContext);
  return useMemo(() => (access ? parseToken(access) : undefined), [access]);
}

export function useAuthenticationApi(): AuthenticationApi {
  return useMemo(
    () =>
      new AuthenticationApi(undefined, import.meta.env.VITE_PUBLIC_API_PREFIX),
    []
  );
}
