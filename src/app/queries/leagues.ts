import { LeaguesManagementApi,TypeaheadResult } from "@digimperial/efoot-admin";
import { useMemo } from "react";
import {
    useMutation,
    UseMutationOptions,
    useQuery,
    useQueryClient,
    UseQueryOptions,
} from "react-query";

import { useApiConfig } from "^/app/utils";


export function useLeaguesApi() {
    const config = useApiConfig();
    return useMemo(() => new LeaguesManagementApi(...config), [config]);
}

export function useLeagueTypeHead(
    search: string,
    options?: UseQueryOptions<TypeaheadResult[]>
  ) {
    const api = useLeaguesApi();
    return useQuery({
      queryKey: ["users", "search", "typeahead", search],
      queryFn: async () => (await api.getLeaguesTypeahead(search)).data,
      ...options,
    });
  }