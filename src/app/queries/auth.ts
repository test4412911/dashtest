import { AuthenticationApi, LoginRequest, TokenPair, UserLevel } from "@digimperial/efoot-auth";
import { isFunction } from "lodash-es";
import { useContext, useMemo } from "react";
import { useCallback } from "react";
import { useMutation, UseMutationOptions, useQuery, useQueryClient } from "react-query";
import { toast } from "react-toastify";

import { AuthContext, setUserData, useApiAuthroizedConfig, useAuthenticationApi, useCurrentUser } from "^/app/utils";

export function useLogin(
  options: UseMutationOptions<TokenPair, unknown, LoginRequest>
) {
  const api = useAuthenticationApi();
  const { setTokens } = useContext(AuthContext);
  const client = useQueryClient();
  const { onSuccess, ...restOptions } = options;
  return useMutation(
    async (request: LoginRequest) => {
      const { data } = await api.authLogin(request);
      return data;
    },
    {
      onSuccess: async (res, data, ctx) => {
        setTokens(res);
        await client.invalidateQueries(["users"]);
        isFunction(onSuccess) && onSuccess(res, data, ctx);
        window.location.replace("/");
      },
      ...restOptions,
    }
  );
}

export function useLogout() {
  const { setTokens } = useContext(AuthContext);

  return useCallback(() => {
    setTokens({});
    setUserData(undefined);
    window.location.replace("/");
  }, [setTokens]);
}


export function useAuthMe(){
  const config = useApiAuthroizedConfig();
  const api = useMemo(() => new AuthenticationApi(...config), [config]);
  return useQuery(["users"], async () => {
    const { data } = await api.authMe();
    return data;
  });
}